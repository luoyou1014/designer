class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nickname
      t.integer :gender
      t.text :introduction
      t.string :pic

      t.timestamps
    end
  end
end
