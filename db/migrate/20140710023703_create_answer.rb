class CreateAnswer < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :user_id
      t.text :content
      t.string :img
      t.integer :votes
      t.integer :had_view

      t.timestamps
    end
  end
end
