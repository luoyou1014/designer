class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :user_id
      t.string :title
      t.text :content
      t.integer :priority
      t.integer :views
      t.integer :answer_num

      t.timestamps
    end
  end
end
