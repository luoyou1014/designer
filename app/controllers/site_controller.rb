class SiteController < ApplicationController
  before_action :require_login, except:['login']

  def login
    @test = 111

    if params[:username] != nil
      user = User.find_by(username: params[:username])
      if user.password == params[:password]
        session[:id] = user.id
        session[:return_url] or session[:return_url] = '/designer/index'
        redirect_to session[:return_url]
      end
    end
  end

end
