class Designer::QuestionController < Designer::BaseController
  def index
    @questions = Question.all
    # @answers = Answer.all
  end

  def has_answer
    @questions = Question.get_question_has_answer(session[:id])
  end

  def reply_me

  end
  
end