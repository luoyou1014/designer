class Question < ActiveRecord::Base
  belongs_to :user
  has_many :answer

  def Question.get_question_has_answer(user_id)
    answers = Answer.where(user_id: user_id).order('id desc')

    questions = Hash.new
    answers.each do |v|
      id = v.question_id
      if questions.include?(id)
        questions[id][:answers].unshift(v.attributes)
      else
        questions[id] = v.question.attributes
        # puts YAML::dump(v.question.attributes)
        questions[id][:answers] = [v.attributes]
      end
    end

    return questions
  end

end
